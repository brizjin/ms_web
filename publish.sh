#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
eval $(docker-machine env ub)
docker-compose build
docker save msweb_web | gzip > ../../msweb_web.docker_image
eval $(docker-machine env msweb)
gunzip -c ../../msweb_web.docker_image | docker load
docker-compose up