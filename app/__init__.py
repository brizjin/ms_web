# -*- coding: utf-8 -*-
import os,sys
from flask import Flask
from app.elastic import Elastic
from sqlalchemy import create_engine

flask_app = Flask(__name__)

flask_app.config.from_object('config.default')
if 'APP_CONFIG_FILE' in os.environ:
	flask_app.config.from_envvar('APP_CONFIG_FILE')
if 'WINDEV' in sys.argv:	
	flask_app.config.from_object('config.windev')

print "v.3"

dbo_db = create_engine(flask_app.config['DB_URL'])
es     = Elastic(flask_app.config['ELASTICSEARCH_URL'])

if flask_app.config['PROFILE']:
	from werkzeug.contrib.profiler import ProfilerMiddleware
	flask_app.wsgi_app = ProfilerMiddleware(flask_app.wsgi_app, restrictions=[50])

from app.views import *
