﻿# -*- coding: utf-8 -*-
from flask import render_template, request, send_from_directory, flash
from app import *
import os, re
from sqlalchemy.sql import text
from flask_wtf import Form
from wtforms.validators import Required
from wtforms import TextField, SelectMultipleField, SubmitField, widgets
from flask.helpers import make_response

phone_re = re.compile('\d{11}')

@flask_app.route("/")
@elastic.cats_request
def index():
    return render_template('random_cat.html')

@flask_app.route("/elba_history", methods=['GET','POST'])
@elastic.cats_request
def elba_history():
    dbo_db.engine.dispose()
    with dbo_db.engine.connect() as conn:
        cur = conn.execute(text(u'''SELECT t.* FROM [dbo].ElbaIntegration t order by id desc'''))
        desc = cur.keys()
        table = cur.fetchall()
        f = {}
    return render_template('table.html', active_nav = 6, table = table, formats = f, encode = os.name != 'posix', desc = desc)

#Потокобезопастное обращение к базе
def execute_fetch(text):
    dbo_db.engine.dispose()
    with dbo_db.engine.connect() as conn:
        res = conn.execute(text).fetchall()
    return res

def execute(text):
    dbo_db.engine.dispose()
    with dbo_db.engine.connect() as conn:
        conn.execute(text)

@flask_app.route("/connect_elba", methods=['GET','POST'])
@elastic.cats_request
def connect_elba():  
    class AddAccForm(Form):
        client  = TextField(u"Клиент ID", validators=[Required()])
        email   = TextField(u"E-mail")
        #connected = SelectMultipleField(u"Подключенны",choices = [('1', 'Choice1'), ('2', 'Choice2'), ('3', 'Choice3')])
        connected    = MultiCheckboxField(u"Подключенны",choices = [])
        disconnected = MultiCheckboxField(u"Отключенны",choices = [])
        submit       = SubmitField(u"Показать",   description = {"class":"btn btn-success form-control"})
        to_conect    = SubmitField(u"Подключить", description = {"class":"btn btn-primary form-control"})
        to_disconct  = SubmitField(u"Отключить",  description = {"class":"btn btn-default form-control"})

    form = AddAccForm()
    

    if form.validate_on_submit() or form.to_conect.data or form.to_disconct.data or form.submit.data:
        pass
        #print "F=",form.connected.data
        #dbo_db.engine.execute(text(u'''insert into [DBO-LW].[dbo].[ElbaIntegration] 
        #                                        select A.owner_ID,A.SHORTNAME, '%s' as 'email', B.acc_number, 1 as 'IS' 
        #                                        from [DBO-LW].[dbo].[bnk_usr_client_ul] A join [DBO-LW].[dbo].[bnk_usr_client_acc] B on A.owner_ID=B.client_id 
        #                                        where B.acc_number='%s' '''%(form.email.data,form.account.data)))
        #flash(u'E-mail успешно добавлен',u'success')

        

        if form.to_conect.data:
            @elastic.cats_request
            def connect():
                integration_sql = u'''insert into [dbo].[ElbaIntegration]
                                           (DBOCompanyID, CompanyName, Email , AccountNumber,IntegrationStatus, DateBeg)
                                    select  A.owner_ID, A.SHORTNAME, '%s'  , B.acc_number , %s               , SYSDATETIME()
                                    from [dbo].[bnk_usr_client_ul] A join [dbo].[bnk_usr_client_acc] B on A.owner_ID=B.client_id
                                    where B.acc_number='%s' '''
                if form.email.data:
                    for acc in form.disconnected.data:
                        execute(text(integration_sql%(form.email.data.strip(),1,acc)))
                else:
                    flash(u'Для подключения счетов необходимо указать EMAIL',u'danger')
            connect()

        if form.to_disconct.data:
            @elastic.cats_request
            def disconnect():
                upsub_sql = '''insert into [dbo].[ElbaIntegration]
                                    (DBOCompanyID, CompanyName, Email , AccountNumber,IntegrationStatus, DateBeg)
                                    select  A.owner_ID
                                        , A.SHORTNAME
                                        ,(select top 1 e.Email from [dbo].ElbaIntegration e where b.client_id = e.dboCompanyId and b.acc_number = e.AccountNumber and e.IntegrationStatus = 1 order by e.DateBeg desc)
                                        , B.acc_number
                                        , 0
                                        , SYSDATETIME()
                                    from [dbo].[bnk_usr_client_ul] A join [dbo].[bnk_usr_client_acc] B on A.owner_ID=B.client_id
                                    where B.acc_number='%s' '''
                for acc in form.connected.data:
                    execute(text(upsub_sql%(acc)))
            disconnect()

        sql_connected = u'''select b.acc_number id, b.acc_number + ' | ' + (select top 1 e.Email from [dbo].ElbaIntegration e where b.client_id = e.dboCompanyId and b.acc_number = e.AccountNumber order by e.DateBeg desc)
                        from [dbo].[bnk_usr_client_acc] B
                        inner join [DBO].[dbo].[ups_doc] ud on B.owner_id = ud.id and ud.doc_status = 'new'
                        where b.client_id = '%s'
                          and coalesce((select top 1 e.integrationStatus from [dbo].ElbaIntegration e where b.client_id = e.dboCompanyId and b.acc_number = e.AccountNumber order by e.DateBeg desc),0) = %s
                            '''
        sql_disconnected = u'''select b.acc_number id, b.acc_number
                        from [dbo].[bnk_usr_client_acc] B
                        inner join [DBO].[dbo].[ups_doc] ud on B.owner_id = ud.id and ud.doc_status = 'new'
                        where b.client_id = '%s'                          
                          and coalesce((select top 1 e.integrationStatus from [dbo].ElbaIntegration e where b.client_id = e.dboCompanyId and b.acc_number = e.AccountNumber order by e.DateBeg desc),0) = %s
                            '''
                                
        form.connected.choices    = execute_fetch(text(sql_connected   %(form.client.data,'1')))
        form.disconnected.choices = execute_fetch(text(sql_disconnected%(form.client.data,'0')))
        
    #else:
    #    print "NOT SUBMIT"
    #    form.client.data = "191219"
    
    

    f = {}
    return render_template('table_acc.html', active_nav=6, formats=f, form=form, encode=True)


@flask_app.route('/fonts/<path:filename>')
def send_foo(filename):
    return send_from_directory(os.path.join(flask_app.config["BASEDIR"],'app','static','fonts'), filename)


class MultiCheckboxField(SelectMultipleField):
    """
    A multiple-select, except displays a list of checkboxes.

    Iterating the field will produce subfields, allowing custom rendering of
    the enclosed checkbox fields.
    """
    widget = widgets.ListWidget(prefix_label=False)
    option_widget = widgets.CheckboxInput()