# -*- coding: utf-8 -*-
from elasticsearch import Elasticsearch
import gevent
from gevent import monkey; monkey.patch_all()
from flask import request, copy_current_request_context
from datetime import datetime

es = None

def Elastic(url):
    global es
    es = Elasticsearch(url)

from functools import wraps
def cats_request(f):
    @wraps(f)
    def cats_decorator(*args, **kwargs):
        start_time = datetime.utcnow()
        ex = None
        try:
            rez = f(*args, **kwargs)
        except Exception as e:
            ex = e
        finally:
            @copy_current_request_context
            def send():
                try:
                    end_time = datetime.utcnow()
                    es.index(index="cats", doc_type="requests", body={"page": f.func_name
                        , "timestamp": end_time
                        , "user_agent": request.headers.get('User-Agent')
                        , 'ip': request.remote_addr
                        , 'exception': ex.message if ex else ""  # json.dumps(sys.exc_info())
                        , 'start_time': start_time
                        , 'elapsed_time': (end_time - start_time).total_seconds()})
                except:
                    print "Не удается отправить сообщение"
            gevent.spawn(send)
        if ex:
            raise
        else:
            return rez
    return cats_decorator