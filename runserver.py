﻿from app import flask_app

if __name__ == "__main__":
    HOST = flask_app.config["HOST"]
    PORT = int(flask_app.config["PORT"])
    flask_app.run(HOST, PORT)
