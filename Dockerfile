FROM python:2-alpine
COPY requirements.txt /code/requirements.txt
WORKDIR /code
RUN apt-get update
RUN apt-get install -y freetds-dev
# RUN apt-get install -y python-dev
# RUN apt-get install -y python-distribute
RUN apt-get install -y gcc
RUN printf '[dbo-db.brc.local]\n\thost = 172.21.9.43\n\tport = 1433\n\ttds version = 7.0' >> /etc/freetds/freetds.conf
RUN pip install -r requirements.txt
COPY . /code
CMD python runserver.py
EXPOSE 5000