# -*- coding: utf-8 -*-
import os

from urllib import quote_plus as urlquote

DEBUG 		 = True
PROFILE  	 = False

HOST                = os.environ.get('SERVER_HOST', '0.0.0.0')
PORT                = os.environ.get('SERVER_PORT', '5000')
ELASTICSEARCH_URL   = os.environ.get('ELASTICSEARCH_URL', 'localhost:9200')
DB_URL              = os.environ.get('DBO_URL','mssql+pymssql://DBO_SQL:%s@dbo-db.brc.local:1433/DBO'% urlquote('Aga5Jew!NfdmP%ewt4d4'))
SECRET_KEY          = os.urandom(24)
BASEDIR             = os.path.abspath(os.path.dirname(os.path.dirname(__file__))) #путь к папке проекта
